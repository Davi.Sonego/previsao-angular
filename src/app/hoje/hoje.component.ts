import { ClimaService } from '../clima.service';
import { Component, Input, OnInit, Output } from '@angular/core';
import { pluck } from 'rxjs/operators';

@Component({
  selector: 'app-hoje',
  templateUrl: './hoje.component.html',
  styleUrls: ['./hoje.component.scss']
})
export class HojeComponent implements OnInit {

  dataClima:any = [];
  detalhesPrevisao:any;

  primeiroDisplay = true;
  segundoDisplay = false;

  indexSelecionado:number | undefined;

  cidade:any;
  timeline : any[] = [];
  climaAtual:any;
  localizacao:any;
  horarioAtual = new Date();
  constructor(private climaService: ClimaService) { }

  ngOnInit(): void {
    this.climaService.getPrevisaoTempo().subscribe(data=>{
      this.getPrevisaoDia(data)
    }),
    this.climaService.getPrevisaoTempo().pipe(
      pluck('list')
    )
    .subscribe(data=>{
      this.previsaoFuturo(data)
    })
  }

  previsaoFuturo(data:any){
    for(let i = 0; i < data.length; i = i + 8){
      this.dataClima.push(data[i]);
    }
  }

  toggle(data:any, index:number){
    this.primeiroDisplay = !this.primeiroDisplay;
    this.segundoDisplay = !this.segundoDisplay;

    this.detalhesPrevisao = data;
    this.indexSelecionado = index;
  }

  detalhes(data:any, i:number){
    this.detalhesPrevisao = data;
    this.indexSelecionado = i;
  }

  getCidadeFuturo(cidade:any){
    this.climaService.getPrevisaoPorCidade(cidade).pipe(
      pluck('list')
    )
    .subscribe(data=>{
      this.dataClima = [];
      this.previsaoFuturo(data);
    })
  }


  intervalo(){
    const start = new Date();
    start.setHours(start.getHours()+(start.getTimezoneOffset() / 60));
    const to = new Date(start);
    to.setHours(to.getHours() + 2, to.getMinutes() + 59, to.getSeconds() + 59);

    return { start, to }
  }

  getPrevisaoDia(hoje:any){
    this.localizacao = hoje.city;

    for(const previsao of hoje.list.slice(0, 8)) {
      this.timeline.push({
        time: previsao.dt_txt,
        temp: previsao.main.temp
      });

      const apiDate = new Date(previsao.dt_txt).getTime();

      if(this.intervalo().start.getTime() <= apiDate && this.intervalo().to.getTime() >= apiDate){
        this.climaAtual = previsao;
      }
    }
  }

  getCidadeHoje(cidade:any){
    this.climaService.getPrevisaoPorCidade(cidade).subscribe(data=>{
      console.log(data);
      this.timeline.length = 0;
      this.getPrevisaoDia(data);
    })
  }

}
