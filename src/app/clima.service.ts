import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable} from '@angular/core';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ClimaService {

  url = 'https://api.openweathermap.org/data/2.5/forecast';

  constructor(private http: HttpClient) { }

getPrevisaoTempo(){

  return new Observable((observer)=>{
    navigator.geolocation.getCurrentPosition(
      (posicao)=>{
        observer.next(posicao)
      },
      (error)=>{
        observer.next(error)
      }
    )
  }).pipe(
    map((value:any)=>{
      return new HttpParams()
        .set('lon', value.coords.longitude)
        .set('lat', value.coords.latitude)
        .set('units', 'metric')
        .set('appid', '02bc8677a572a9848a043da218fd1a3c')
    }),
    switchMap((values)=>{
      return this.http.get('https://api.openweathermap.org/data/2.5/forecast', { params: values})
    })
  )

}

getPrevisaoPorCidade(cidade: string){
  let params = new HttpParams()
  .set('q', cidade)
  .set('units', 'metric')
  .set('appid', '02bc8677a572a9848a043da218fd1a3c')

  return this.http.get(this.url, {params});
}


}
